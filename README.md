# Handful

A web based 3D visualiser for arm orientation and hand gestures from a Myo armband. The project was a university dissertation project for the now discontinued Myo armband device and SDK.

The project was made as an exercise in learning machine learning methods and analysis procedures to develop models. In order to detect hand gestures, the gesture data must be collected from the desired user beforehand.

(Note: As the project was largely a learning exercise in machine learning fundamentals, the data collection processes and files are not neatly organised or the most efficient and user friendly. This will not be improved in the future due to the Myo armband being discontinued)

## Requirements

This project has only been tested on MacOS 10.14 Mojave using the MacOS versions of the Myo SDK and Myo Connect.

-   Myo Connect - with a Myo armband device
-   Myo armband SDK (0.9)
-   Python3
-   NodeJS

## Retraining

The machine learning was optimised for 6 hand gesture classes (however any others may be added or used)

-   'fist'
-   'open'
-   'pinch'
-   'rest'
-   'wave-in'
-   'wave-out'

(Hand gesture icons were made for the hand gesture classes as above and so any other classes used will not show an icon in the visualiser)

### Data Collection

Gesture data is collected via the armband and SDK and stored in CSV files within the `/server/data` directory.

To begin collecting data first edit `/server/collector.py` with the desired `label` and time to spend collecting (`collection_dur`). Then run the collector script after the gesture is performed by the user. I recommend running the collection for each label multiple times for more reliable results.

```bash
python3 server/collector.py
```

### Training with classifiers

The training phase takes place on running the server script and the trained models are not saved. Adding more data will increase this start up time of the server as it has to classify the collected gesture data.

Any of the following classifiers may be selected from the classifier classes within their respective python files instead of the default `KNN` class classifier on line `123`.

```python
classifier = KNN(X_train, y_train)
# OR
classifier = LDA(X_train, y_train)
# etc.
```

##### Classifiers:

-   `KNN`
-   `LDA`
-   `MLP`
-   `SVM`

## Running

Move the Myo SDK to the root of the repo into a `SDK/` folder

Install the python requirements from `requirements.txt` in the root of the repo for the backend server

```bash
pip3 install -r requirements.txt
```

Install and build the web client from `package.json` required for serving the web client

```bash
npm install
npm run build
```

Ensure that Myo Connect is running with a registered and connected Myo armband device (can be done by following the instructions in the Myo Connect app on startup) and that the device is warmed up and worn by a user.

Then start the python server from the root of the project repo and visit either `http://localhost:8080` on the machine running the server or from another device on port `8080` of the server to view the visualiser
