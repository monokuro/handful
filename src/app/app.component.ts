import { Component, OnInit } from '@angular/core'
import * as THREE from 'three'

import { map } from 'rxjs/operators'

import { SocketService } from './socket.service'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
	armOrientation: THREE.Quaternion
	currentGesture: string = 'rest'

	constructor(private socketService: SocketService) {}

	ngOnInit() {
		this.socketService
			.getOrientationStream()
			.pipe(
				map(orientation => {
					return new THREE.Quaternion(
						orientation.x,
						orientation.y,
						orientation.z,
						orientation.w,
					)
				}),
			)
			.subscribe(orientation => (this.armOrientation = orientation))

		this.socketService.getGestureStream().subscribe(gesture => {
			this.currentGesture = gesture
		})
	}
}
