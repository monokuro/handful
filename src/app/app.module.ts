import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { ArmDisplayComponent } from './arm-display/arm-display.component'

import { environment as env } from '../environments/environment'

const config: SocketIoConfig = { url: env.SOCKET_URL, options: {} }

@NgModule({
	declarations: [AppComponent, ArmDisplayComponent],
	imports: [BrowserModule, AppRoutingModule, SocketIoModule.forRoot(config)],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
