import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArmDisplayComponent } from './arm-display.component';

describe('ArmDisplayComponent', () => {
  let component: ArmDisplayComponent;
  let fixture: ComponentFixture<ArmDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArmDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArmDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
