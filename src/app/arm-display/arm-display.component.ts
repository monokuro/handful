import {
	Component,
	OnInit,
	ViewChild,
	ElementRef,
	HostListener,
	Input,
} from '@angular/core'
import * as THREE from 'three'

@Component({
	selector: 'arm-display',
	templateUrl: './arm-display.component.html',
	styleUrls: ['./arm-display.component.sass'],
})
export class ArmDisplayComponent implements OnInit {
	@ViewChild('canvas') displayContainer: ElementRef
	scene = new THREE.Scene()
	camera: THREE.PerspectiveCamera
	axesHelper = new THREE.AxesHelper(5)
	private renderer = new THREE.WebGLRenderer()
	private viewInitialized = false

	private geometry = new THREE.BoxGeometry(5, 2, 2).translate(-2, 0, 0)
	private material = new THREE.MeshBasicMaterial({ color: 0x00ff00 })
	armModel = new THREE.Mesh(this.geometry, this.material).rotateY(
		-Math.PI / 2,
	)

	// Drone model rotational inputs
	@Input()
	set alpha(val: number) {
		this.armModel.rotation.y = val
	}
	@Input()
	set beta(val: number) {
		this.armModel.rotation.x = val
	}
	@Input()
	set gamma(val: number) {
		this.armModel.rotation.z = val
	}
	@Input()
	set quaternion(val: THREE.Quaternion) {
		// this.armModel.quaternion.set(val.x, val.y, val.z, val.w)
		this.armModel.setRotationFromQuaternion(val)
	}

	constructor() {}

	animate = () => {
		// Animate loop
		requestAnimationFrame(this.animate)
		this.render()
	}

	ngOnInit() {}

	ngAfterViewInit() {
		this.viewInitialized = true
		this.camera = new THREE.PerspectiveCamera(
			75,
			this.canvas.clientWidth / this.canvas.clientHeight,
			0.1,
			1000,
		)
		// Configure arm up vector
		this.scene.add(this.armModel)
		this.scene.add(this.axesHelper)

		this.camera.position.z = 10

		this.startRendering()
	}

	public get renderPane(): ElementRef {
		return this.displayContainer
	}

	private get canvas(): HTMLCanvasElement {
		return this.displayContainer.nativeElement
	}

	private startRendering() {
		console.log('Started Rendering')
		this.renderer = new THREE.WebGLRenderer({
			canvas: this.canvas,
			antialias: true,
		})
		this.renderer.setPixelRatio(devicePixelRatio)
		this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight)

		this.renderer.shadowMap.enabled = true
		this.renderer.shadowMap.type = THREE.PCFSoftShadowMap
		this.renderer.setClearColor(0xffffff, 1)
		this.renderer.autoClear = true

		this.updateChildCamerasAspectRatio()
		// Trigger loop
		this.animate()
		// this.render()
	}

	public render() {
		if (this.viewInitialized) {
			const sceneComponent = this.scene
			const cameraComponent = this.camera

			this.renderer.render(sceneComponent, cameraComponent)
		}
	}

	private calculateAspectRatio(): number {
		const height = this.canvas.clientHeight
		if (height === 0) {
			return 0
		}
		return this.canvas.clientWidth / this.canvas.clientHeight
	}

	// Keep canvas at maximum possible size
	@HostListener('window:resize', ['$event'])
	public onResize(_event: Event) {
		this.canvas.style.height = `${0.45 * window.innerHeight}px`
		this.canvas.style.width = `${window.innerWidth}px`

		this.updateChildCamerasAspectRatio()

		this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight)
		this.render()
	}

	public updateChildCamerasAspectRatio() {
		const aspect = this.calculateAspectRatio()
		// this.cameraComponents.forEach(camera =>
		// 	camera.updateAspectRatio(aspect),
		// )
		this.camera.aspect = aspect
		this.camera.updateProjectionMatrix()
	}
}
