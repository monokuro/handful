import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { ArmDisplayComponent } from './arm-display/arm-display.component'

const routes: Routes = [
	{ path: '', component: ArmDisplayComponent },
	{
		path: '**',
		redirectTo: '',
	},
]

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
