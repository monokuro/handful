import { Injectable } from '@angular/core'
import { Socket } from 'ngx-socket-io'

@Injectable({
	providedIn: 'root',
})
export class SocketService {
	constructor(private socket: Socket) {}

	sendMessage(msg: string) {
		this.socket.emit('message', msg)
	}

	getOrientationStream() {
		return this.socket.fromEvent<any>('arm-motion')
	}

	getGestureStream() {
		return this.socket.fromEvent<any>('hand-gesture')
	}
}
