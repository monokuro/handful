import socketio
import asyncio
import uvloop
import myo
from threading import Lock
from aiohttp import web
import aiofiles, aiohttp

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from myo_band import Myo_Band, Feature
from lda import LDA
from knn import KNN
from svm import SVM
from mlp import MLP

# Asyncio setup and linking with aiohttp app
started = False
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
loop = asyncio.get_event_loop()
sio = socketio.AsyncServer(async_handlers=True, async_mode="aiohttp")
app = web.Application(loop=loop)
sio.attach(app)
routes = web.RouteTableDef()

routes.static(prefix="/assets", path="./dist/handful/assets")

@routes.get("/{path:.*}")
async def v_catch_all(request):
    path = request.match_info.get("path", "/")
    content_type = "text/html"
    if path.endswith(".js"):
        content_type = "application/javascript"
    elif path.endswith(".css"):
        content_type = "text/css"
    elif path.endswith(".ico"):
        content_type = "image/x-icon"
    else:
        path = 'index.html'
    if content_type != "text/html" or path == "index.html":
        try:
            async with aiofiles.open(
                f"./dist/handful/{path}", mode="rb"
            ) as resource:
                body = await resource.read()
        except FileNotFoundError as exc:
            app.logger.error(exc)
            body = ""

    resp = web.Response(body=body, content_type=content_type)
    resp.headers[aiohttp.hdrs.CACHE_CONTROL] = f"max-age={60*15}"
    return resp

app.add_routes(routes)

def startup():
    global started
    if started:
        return
    print('Started')
    # Start orientation streaming
    asyncio.ensure_future(fetch_orientation_data(myo_band))
    asyncio.ensure_future(fetch_current_gesture(myo_band))
    started = True

async def fetch_orientation_data(listener):
    # Starts loop returning current orientation states
    while True:
        quaternion = listener.get_orientation()
        await sio.emit("arm-motion", data=quaternion, namespace="/client")
        await asyncio.sleep(0.1)


async def fetch_current_gesture(band: Myo_Band):
    # Starts loop returning currnet gesture state
    while True:
        current_gesture = band.get_gesture()
        await sio.emit("hand-gesture", data=current_gesture, namespace="/client")
        await asyncio.sleep(0.5)


# SocketIO events
@sio.on("connect", namespace="/client")
async def connect(sid, environ):
    print("Connected ", sid)
    startup()


@sio.on("disconnect", namespace="/client")
async def disconnect(sid):
    print("Disconnected ", sid)


if __name__ == "__main__":
    # Myo setup
    myo.init(sdk_path="./sdk")
    hub = myo.Hub()

    # Setup
    LABELS = ["rest", "fist", "open", "wave-out", "pinch", "wave-in"]
    datasets = []
    window_size = 10

    # Format data for classifier training
    columns = [str(col) for col in range(0, 8)]
    for label in LABELS:
        dataset = pd.read_csv(f"server/data/{label}.csv")
        dataset[columns] = (
            dataset[columns].rolling(axis=0, min_periods=1, window=10).apply(Feature.rms, raw=True)
        )
        datasets.append(dataset.dropna())
    dataset = pd.concat(datasets, keys=LABELS)

    # Setup data for classification
    X = dataset.drop("Class", axis="columns")
    y = dataset["Class"]

    # Split data into training and testing sets according to a percentage
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    classifier = KNN(X_train, y_train)
    classifier.evaluate(X_test, y_test)
    myo_band = Myo_Band(classifier, window_size)

    # Start Myo listener thread and server
    with hub.run_in_background(myo_band.on_event):
        web.run_app(app)
