import pandas as pd

# DATA_FILE_PATH = 'data/rest.csv'
DATA_FILE_PATH = 'data/example.csv'

raw_data = {'first_name': ['Jason', 'Molly', 'Tina', 'Jake', 'Amy'],
            'last_name': ['Miller', 'Jacobson', 'Ali', 'Milner', 'Cooze'],
            'age': [42, 52, 36, 24, 73],
            'preTestScore': [4, 24, 31, 2, 3],
            'postTestScore': [25, 94, 57, 62, 70]}
df = pd.DataFrame(raw_data, columns=['first_name', 'last_name',
                                     'age', 'preTestScore', 'postTestScore'])

# Creating a new file
df.to_csv(DATA_FILE_PATH, index=False)


# Appending new data
# new_data = {'first_name': ['Jenkins'],
#             'last_name': ['Plopper'],
#             'age': [99],
#             'preTestScore': [0],
#             'postTestScore': [100]}
# new_frame = pd.DataFrame(new_data, columns=['first_name', 'last_name',
#                                             'age', 'preTestScore', 'postTestScore'])
# new_frame.to_csv(DATA_FILE_PATH, mode='a', header=False, index=False)


dataset = pd.read_csv(DATA_FILE_PATH)
print(dataset.shape)
print(dataset.head(3))
