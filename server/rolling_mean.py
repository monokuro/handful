import pandas as pd

LABEL = 'rest'
LABEL_DATA_PATH = f'server/data/{LABEL}.csv'

dataset = pd.read_csv(LABEL_DATA_PATH)
# print(dataset.describe())

print(dataset.shape)

columns = [str(col) for col in range(0, 8)]
dataset[columns] = dataset[columns].rolling(window=10).mean()
dataset = dataset.dropna()
print(dataset.shape)
print(dataset.head(10))
