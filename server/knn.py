import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.pipeline import make_pipeline

from myo_band import Feature


class KNN:
    def __init__(self, training_input, training_output, n_neighbors=3):
        self.knnclassifier = KNeighborsClassifier(n_neighbors=n_neighbors)

        # Normalising
        self.scaler = StandardScaler()
        self.scaler.fit(training_input)

        # Remove training step after finding a saving solution
        training_input = self.scaler.transform(training_input)
        self.knnclassifier.fit(training_input, training_output)

    def predict(self, input):
        # Perform and return predicted class label
        normalised_data = self.scaler.transform(input)
        prediction = self.knnclassifier.predict(normalised_data)
        return prediction

    def evaluate(self, testing_input, testing_output):
        # Perform evaluation using confusin matrix and accuracy score
        testing_input = self.scaler.transform(testing_input)
        testing_predictions = self.knnclassifier.predict(testing_input)
        print(confusion_matrix(testing_output, testing_predictions))
        print(classification_report(testing_output, testing_predictions))
        print("Score: ", self.knnclassifier.score(testing_input, testing_output))


if __name__ == "__main__":
    # Import datasets
    LABELS = ["rest", "fist", "open", "wave-out", "pinch", "wave-in"]
    datasets = []

    # Combine gesture data and apply feature extraction method
    columns = [str(col) for col in range(0, 8)]

    for label in LABELS:
        dataset = pd.read_csv(f"server/data/{label}.csv")
        dataset[columns] = (
            dataset[columns]
            .rolling(min_periods=1, window=10)
            .apply(Feature.var, raw=True)
        )
        datasets.append(dataset.dropna())

    dataset = pd.concat(datasets, keys=LABELS)

    # Setup data for classification
    X = dataset.drop("Class", axis="columns")
    y = dataset["Class"]

    # Split data into training and testing sets according to a percentage
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.20, random_state=1, stratify=y
    )

    # Normalise the datasets
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    # KNN training
    classifier = KNeighborsClassifier(n_neighbors=3)
    classifier.fit(X_train, y_train)

    # Predictions for testing
    y_pred = classifier.predict(X_test)

    # Basic trained model evaluation
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    print("Model Score: ", classifier.score(X_test, y_test))

    # Cross evaluation
    knn_cv = make_pipeline(StandardScaler(), KNeighborsClassifier(n_neighbors=3))
    cv_scores = cross_val_score(knn_cv, X, y, cv=5, n_jobs=-1)
    print(cv_scores)
    print("Cross Validation Mean Score: ", np.mean(cv_scores))

    # Dtermining K-Value
    error = []

    # Calculating error for K values between 1 and 50
    max_k = 50
    for k_val in range(1, max_k):
        knn = KNeighborsClassifier(n_neighbors=k_val)
        knn.fit(X_train, y_train)
        pred_i = knn.predict(X_test)
        error.append(np.mean(pred_i != y_test))

    plt.figure(figsize=(12, 6))
    plt.plot(
        range(1, max_k),
        error,
        color="red",
        linestyle="dashed",
        marker="o",
        markerfacecolor="blue",
        markersize=10,
    )
    plt.title("K-Value Error rates")
    plt.xlabel("K Value")
    plt.ylabel("Mean Error")
    plt.show()
