import pandas as pd
import numpy as np
import myo
from time import sleep

from os import path, remove
from threading import Lock


class DataStore:
    """docstring for Collector."""

    def __init__(self, data_directory="server/data"):
        self.data_directory = data_directory

    def clear(self, label: str):
        file_path = path.join(self.data_directory, f"{label}.csv")
        if path.isfile(file_path):
            remove(file_path)

    def save_snapshot(self, data, label: str):
        # Setup
        file_path = path.join(self.data_directory, f"{label}.csv")

        # Generate data frame to store
        data.append(label)  # Add classifier label to set
        data = np.array([data])  # Convert data to a vector
        data_row = pd.DataFrame(data).rename(index=str, columns={8: "Class"})

        # Create CSV file with headings if one doesn't exist already
        if not path.isfile(file_path):
            data_row.to_csv(file_path, index=False)

        # Store data frame in file
        data_row.to_csv(file_path, mode="a", header=False, index=False)

    def get_dataset(self, label: str):
        return pd.read_csv(path.join(self.data_directory, f"{label}.csv"))


class EmgCollector(myo.DeviceListener):
    """
    Collects live EMG streaming data
    """

    def __init__(self, data_store: DataStore, emg_classifier_label: str):
        self.lock = Lock()
        self.data_store = data_store
        self.emg_classifier_label = emg_classifier_label
        self.emg_data = []

    def get_emg_data(self):
        with self.lock:
            return self.emg_data

    def on_connected(self, event):
        event.device.stream_emg(True)

    def on_emg(self, event):
        with self.lock:
            self.emg_data = event.emg
            self.data_store.save_snapshot(event.emg, self.emg_classifier_label)


if __name__ == "__main__":
    myo.init(sdk_path="./sdk")
    hub = myo.Hub()
    label = "rest"

    data_store = DataStore()
    listener = EmgCollector(data_store, label)

    collection_dur = 5  # Seconds

    # Clear store for label
    # data_store.clear(label)
    with hub.run_in_background(listener.on_event):
        # Collect EMG data for a set duration
        sleep(collection_dur)
        print(f"Finished collecting data for label: {label}")
