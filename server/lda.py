import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as lda
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.pipeline import make_pipeline

from myo_band import Feature

# Class
class LDA:
    def __init__(self, training_input, training_output, n_components=1):
        self.ldaclassifier = lda(n_components=n_components)

        # Normalising
        self.scaler = StandardScaler()
        self.scaler.fit(training_input)

        # Remove training step after finding a saving solution
        training_input = self.scaler.transform(training_input)
        self.ldaclassifier.fit(training_input, training_output)

    def predict(self, input):
        normalised_data = self.scaler.transform(input)
        prediction = self.ldaclassifier.predict(normalised_data)
        return prediction

    def evaluate(self, testing_input, testing_output):
        testing_input = self.scaler.transform(testing_input)
        testing_predictions = self.ldaclassifier.predict(testing_input)
        print(confusion_matrix(testing_output, testing_predictions))
        print(classification_report(testing_output, testing_predictions))
        print("Score: ", self.ldaclassifier.score(testing_input, testing_output))


if __name__ == "__main__":
    # Import datasets
    LABELS = ["rest", "fist", "open", "wave-out", "pinch", "wave-in"]
    datasets = []

    columns = [str(col) for col in range(0, 8)]

    for label in LABELS:
        dataset = pd.read_csv(f"server/data/{label}.csv")
        dataset[columns] = (
            dataset[columns].rolling(min_periods=1, window=10).apply(Feature.rms, raw=True)
        )
        datasets.append(dataset.dropna())

    dataset = pd.concat(datasets, keys=LABELS)

    # Setup data for classification
    X = dataset.drop("Class", axis="columns")
    y = dataset["Class"]

    # Split data into training and testing sets according to a percentage
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    # Normalising
    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    # LDA Training
    ldaclassifier = lda(n_components=3)
    ldaclassifier.fit(X_train, y_train)

    # Prediction
    y_pred = ldaclassifier.predict(X_test)

    # Evaluation
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    print("Model Score: ", ldaclassifier.score(X_test, y_test))

    # Cross evaluation
    lda_cv = make_pipeline(StandardScaler(), lda(n_components=3))
    cv_scores = cross_val_score(lda_cv, X, y, cv=5, n_jobs=-1)
    # print each cv score (accuracy) and average them
    print(cv_scores)
    print("Cross Validation Mean Score: ", np.mean(cv_scores))
