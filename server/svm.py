import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.pipeline import make_pipeline

from myo_band import Feature

# Class
class SVM:
    def __init__(self, training_input, training_output, C=50, gamma=1):
        self.svclassifier = SVC(C=C, gamma=gamma)

        # Normalising
        self.scaler = StandardScaler()
        self.scaler.fit(training_input)

        # Remove training step after finding a saving solution
        training_input = self.scaler.transform(training_input)
        self.svclassifier.fit(training_input, training_output)

    def predict(self, input):
        normalised_data = self.scaler.transform(input)
        prediction = self.svclassifier.predict(normalised_data)
        return prediction

    def evaluate(self, testing_input, testing_output):
        testing_input = self.scaler.transform(testing_input)
        testing_predictions = self.svclassifier.predict(testing_input)
        print(confusion_matrix(testing_output, testing_predictions))
        print(classification_report(testing_output, testing_predictions))
        print("Score: ", self.svclassifier.score(testing_input, testing_output))


if __name__ == "__main__":
    # Import datasets
    LABELS = ["rest", "fist", "open", "wave-out", "pinch", "wave-in"]
    datasets = []

    columns = [str(col) for col in range(0, 8)]

    for label in LABELS:
        dataset = pd.read_csv(f"server/data/{label}.csv")
        dataset[columns] = (
            dataset[columns].rolling(min_periods=1, window=10).apply(Feature.var, raw=True)
        )
        datasets.append(dataset.dropna())

    dataset = pd.concat(datasets, keys=LABELS)

    # Setup data for classification
    X = dataset.drop("Class", axis="columns")
    y = dataset["Class"]

    # Split data into training and testing sets according to a percentage
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    # Normalising
    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    # SVM Training
    svclassifier = SVC(C=50, gamma=1, kernel="rbf")
    svclassifier.fit(X_train, y_train)

    # Prediction
    y_pred = svclassifier.predict(X_test)

    # Evaluation
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    print("Model Score: ", svclassifier.score(X_test, y_test))

    # Cross evaluation
    svm_cv = make_pipeline(StandardScaler(), SVC(C=50, gamma=1, kernel="rbf"))
    cv_scores = cross_val_score(svm_cv, X, y, cv=5, n_jobs=-1)
    # print each cv score (accuracy) and average them
    print(cv_scores)
    print("Cross Validation Mean Score: ", np.mean(cv_scores))

    # # Grid Search
    # # Parameter Grid
    # param_grid = {'C': [1, 5, 10, 50, 100, 200], 'gamma': [1, 0.1, 0.01, 10]}
    #
    # # Make grid search classifier
    # clf_grid = GridSearchCV(SVC(), param_grid, verbose=1, n_jobs=3)
    #
    # # Train the classifier
    # clf_grid.fit(X_train, y_train)
    #
    # print("Best Parameters:\n", clf_grid.best_params_)
    # print("Best Estimators:\n", clf_grid.best_estimator_)
