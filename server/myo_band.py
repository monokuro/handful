import numpy as np
import myo
from threading import Lock
from collections import deque


class Feature:
    @staticmethod
    def mean(val):
        return np.mean(val, axis=0)

    @staticmethod
    def rms(val):
        return np.sqrt(np.mean(val ** 2, axis=0))

    @staticmethod
    def var(val):
        return np.var(val, axis=0)

    @staticmethod
    def mav(val):
        return np.mean(np.absolute(val), axis=0)


class Feature_Extractor:
    def __init__(self, window_size=10):
        self.window_size = window_size
        self.data_window = deque(maxlen=window_size)

    def append_data(self, data):
        # Append new data to the end of the deque object
        self.data_window.append(data)

    def mean(self):
        return [Feature.mean(self.data_window)]

    def rms(self):
        return [Feature.rms(self.data_window)]

    def var(self):
        return [Feature.var(self.data_window)]

    def mav(self):
        return [Feature.mav(self.data_window)]


class Myo_Band(myo.DeviceListener):
    def __init__(self, classifier, window_size=10):
        self.lock = Lock()
        self.classifier = classifier
        self.feature_extractor = Feature_Extractor(window_size)
        self.orientation = {"x": 0, "y": 0, "z": 0, "w": 0}

    def on_connected(self, event):
        event.device.stream_emg(True)

    def on_orientation(self, event):
        # Update orientation state every event
        with self.lock:
            self.orientation = event.orientation

    def on_emg(self, event):
        # Store EMG data every event
        with self.lock:
            self.feature_extractor.append_data(event.emg)

    def _predict_gesture(self, feature_data) -> str:
        # Uses provided classifier to get the most likely classification prediction
        gesture = self.classifier.predict(feature_data)[0]
        return gesture

    def get_orientation(self):
        # Returns last orientation state
        with self.lock:
            return {
                "x": self.orientation.x,
                "y": self.orientation.y,
                "z": self.orientation.z,
                "w": self.orientation.w,
            }

    def get_gesture(self) -> str:
        # Returns current gesture prediction
        with self.lock:
            feature_data = self.feature_extractor.var()
            return self._predict_gesture(feature_data)
